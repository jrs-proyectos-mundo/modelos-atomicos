let ELEMENTS = []

fetch('https://neelpatel05.pythonanywhere.com/')
  .then(res => {
    if (res.ok) return res.json()
  })
  .then(elements => {
    ELEMENTS = elements
    const controls = new Controls()
    controls.messageBody("Se han cargado los elementos", 'success')
  })

class Tools {
  static validator(data, type, title, node_error = null) {
    let error = "";
    // Validando que no este vacio
    // TODO: Realizar un TRIM
    if (data.length > 0) {
      // Validando el tipo de dato
      if (typeof data == type) {
        return true;
      } else error = `${title}: Debe de ser de tipo ${type}`;
    } else error = `${title}: No puede estar vacio`;

    console.error(error);
    return false;
  }
}

/*
 * Clases para el área del Elemento
 */
class ElementActionsListeners {
  element_list_click(data) {
    this.set_letter = data.symbol
    this.set_atomic = data.atomicNumber
    this.set_mass = Math.round(data.atomicMass.substring(0, 5))


    const body = document.getElementById('body')
    const elements = document.getElementById('modal_elements')
    elements.classList.add('content__modal__elements--close')
    setTimeout(() => {
      body.removeChild(elements)
    }, 500)
  }

  btn_search_click() {
    this.data_search.addEventListener('click', () => {
      const body = document.getElementById('body')
      const elements = document.createElement('div')
      elements.id = "modal_elements"
      elements.classList.add('content__modal__elements')
      body.appendChild(elements)

      const elementsTitle = document.createElement('h3')
      elementsTitle.textContent = "Escoge un elemento a analizar"
      elements.appendChild(elementsTitle)

      const elementsList = document.createElement('div')
      elementsList.classList.add('content__modal__elements__list')
      elements.appendChild(elementsList)

      const closeModal = document.createElement('span')
      closeModal.classList.add('content__modal__elements__close')
      closeModal.textContent = "cerrar"
      elements.appendChild(closeModal)
      closeModal.addEventListener('click', e => {
        elements.classList.add('content__modal__elements--close')
        setTimeout(() => {
          body.removeChild(elements)
        }, 500)
      })

      ELEMENTS.forEach(element => {
        const elementRow = document.createElement('div')
        elementRow.classList.add('content__modal__elements__list__row')
        elementsList.appendChild(elementRow)

        const elementSymbol = document.createElement('span')
        elementSymbol.textContent = element.symbol
        elementRow.appendChild(elementSymbol)

        const elementName = document.createElement('span')
        elementName.textContent = element.name
        elementRow.appendChild(elementName)

        const elementState = document.createElement('span')
        elementState.textContent = element.standardState ? element.standardState : '---'
        elementRow.appendChild(elementState)

        elementRow.addEventListener('click', () => this.element_list_click(element))
      })

    })
  }
}
class ElementActions extends ElementActionsListeners {
  constructor() {
    super()
  }
  get get_atomic() {
    const data = this.data_atomic.value;
    if (Tools.validator(data, "string", "Número Atomico", "")) return data;
    return null;
  }

  get get_letter() {
    const data = this.data_letter.value;
    if (Tools.validator(data, "string", "Elemento", "")) return data;
    return null;
  }

  get get_mass() {
    const data = this.data_mass.value;
    if (Tools.validator(data, "string", "Masa Atomica", "")) return data;
    return null;
  }

  set set_atomic(data) {
    this.data_atomic.value = data
  }

  set set_letter(data) {
    this.data_letter.value = data
  }

  set set_mass(data) {
    this.data_mass.value = data
  }

  get_neutrons(number = false) {
    let res = "";

    const atomic = this.get_atomic;
    const mass = this.get_mass;
    if (atomic != null && mass != null) {
      const electrons = atomic;
      const rand_mass = Math.round(mass);
      const neutrons = rand_mass - electrons;

      if (number) res = neutrons;
      else res = `${rand_mass} - ${electrons} = ${neutrons}`;
    }

    return res;
  }

  setFocusAtomic() {
    this.data_atomic.focus()
  }
  setFocusLetter() {
    this.data_letter.focus()
  }
  setFocusMass() {
    this.data_mass.focus()
  }
}
class Element extends ElementActions {
  data_atomic = null;
  data_letter = null;
  data_mass = null;

  data_search = null

  constructor() {
    super();
    this.data_atomic = document.getElementById("data_atomic");
    this.data_letter = document.getElementById("data_letter");
    this.data_mass = document.getElementById("data_mass");
    this.data_search = document.getElementById("data_search")
  }
}
new Element().btn_search_click()

/*
 * Clases para el área de los Controles
 */
class ControlsListeners {
  information = null;

  constructor() {
    this.information = new Information();
  }

  btn_info_click() {
    this.btn_info.addEventListener("click", () => {
      const element = new Element();
      const letter = element.get_letter;
      const atomic = element.get_atomic;
      const mass = element.get_mass;
      const neutrons = element.get_neutrons();

      if (atomic == null || mass == null || letter == null) {
        this.messageBody("Es necesario llenar los campos del elemento a analizar")

        if (atomic == null)
          element.setFocusAtomic()
        else if (letter == null)
          element.setFocusLetter()
        else if (mass == null)
          element.setFocusMass()

        return
      }

      this.information.generate_info()
      this.information.set_z(atomic);
      this.information.set_p(atomic);
      this.information.set_e(atomic);
      this.information.set_n(neutrons);

      this.btn_clear.disabled = false;

      // Activar los botones de los módelos
      document.getElementById("btn_quantum").disabled = false;
      document.getElementById("btn_bohr").disabled = false;
    });
  }

  btn_clear_click() {
    this.btn_clear.addEventListener("click", () => {

      try {
        this.information.set_z("");
        this.information.set_p("");
        this.information.set_e("");
        this.information.set_n("");

        this.btn_clear.disabled = true;

        document.getElementById('info_element').innerHTML = ""
        document.getElementById("btn_quantum").disabled = true;
        const table_quantic = document.getElementById("quantum_table")
        if (table_quantic != null)
          table_quantic.innerHTML = "";
        const ope_quantic = document.getElementById("quantum_operations")
        if (ope_quantic != null)
          ope_quantic.innerHTML = "";
        const model_quantic = document
          .getElementById("model_quantic")
        const box_quantic = document.getElementById("box_quantic")
        if (box_quantic != null)
          model_quantic.removeChild(box_quantic);
        const quantum_header = document.getElementById('quantum_header')
        quantum_header.innerHTML = ""

        document.getElementById("bohr_levels").innerHTML = "";
        document.getElementById("btn_bohr").disabled = true;
        const model_bohr = document
          .getElementById("model_bohr")
        const box_bohr = document.getElementById("box_bohr")
        if (box_bohr != null)
          model_bohr.removeChild(box_bohr);
        const bohr_header = document.getElementById('bohr_header')
        if (bohr_header != null)
          model_bohr.removeChild(bohr_header);

        const bohr_levels = document.getElementById("bohr_levels");
        Object.keys(bohr_levels.children).forEach((row) => {
          if (row > 0) {
            bohr_levels.children[row].children[1].textContent = "";
            bohr_levels.children[row].children[2].textContent = "";
          }
        });

        document.getElementById("data_atomic").value = "";
        document.getElementById("data_letter").value = "";
        document.getElementById("data_mass").value = "";
        document.getElementById("data_letter").focus();
      } catch (error) {
        console.error(error)
      }
    });
  }
}
class ControlsActions extends ControlsListeners {
  constructor() {
    super()
  }

  messageBody(message, type = "error") {
    const body = document.getElementById("body")

    try {
      body.removeChild(document.getElementById('message_body'))
    } catch (error) {}

    const span = document.createElement('span')
    span.id = "message_body"
    body.appendChild(span)
    span.textContent = message

    switch (type) {
      case 'success':
        span.classList.add("message__error")
        span.classList.add("message__error--success")
        break;
      case "error":
        span.classList.add("message__error")
        break;
    }

    setTimeout(() => {
      span.classList.add("message__error--hidden")
    }, 5000)
  }
}
class Controls extends ControlsActions {
  btn_info = null;
  btn_clear = null;

  constructor() {
    super();
    this.btn_info = document.getElementById("btn_info");
    this.btn_info_click();

    this.btn_clear = document.getElementById("btn_clear");
  }
}

/*
 * Clases para el área de la Información
 */
class InformationActions {
  set_z(value) {
    const z = document.getElementById("info_z")
    if (z != null)
      z.textContent = value
  }

  set_p(value) {
    const p = document.getElementById("info_p")
    if (p != null)
      p.textContent = value
  }

  set_e(value) {
    const e = document.getElementById("info_e")
    if (e != null)
      e.textContent = value
  }

  set_n(value) {
    const n = document.getElementById("info_n")
    if (n != null)
      n.textContent = value
  }

  generate_info() {
    this.info = document.getElementById('info_element')
    this.info.innerHTML = ""

    // Creando el título
    const title = document.createElement('h3')
    this.info.appendChild(title)

    // Creando la caja informativa
    const box = document.createElement('div')
    box.classList.add('content__data__info__box')
    this.info.appendChild(box)

    // Creando las filas
    const rows = ['Z =', '# Protones =', '# Electrones =', '# Neutrones =']
    const rows_ids = ['info_z', 'info_p', 'info_e', 'info_n']

    for (let i = 0; i < rows.length; i++) {
      const row = document.createElement('div')
      row.classList.add('content__data__info__box__row')
      box.appendChild(row)

      const label = document.createElement('label')
      label.classList.add('content__data__info__box__row__label')
      label.setAttribute('for', 'data_atomic')
      label.textContent = rows[i]
      row.appendChild(label)

      const span = document.createElement('span')
      span.classList.add('content__data__info__box__row__value')
      if (i % 2 != 0)
        span.classList.add('content__data__info__box__row__value--even')
      span.id = rows_ids[i]
      row.appendChild(span)
    }
  }

  selection_files() {
    // Seleccionando IDS
    this.z = document.getElementById("info_z");
    this.p = document.getElementById("info_p");
    this.e = document.getElementById("info_e");
    this.n = document.getElementById("info_n");
  }
}
class Information extends InformationActions {
  z = null;
  p = null;
  e = null;
  n = null;

  info = null

  constructor() {
    super();
  }
}
const controls = new Controls();
controls.btn_info_click()
controls.btn_clear_click()

/*
 * Clases para el área del canvas del bohr
 */
class CanvasBohrActions {
  ctx = null;

  element = null;

  levelsElectrons = {};

  bgStage = "lightgrey";
  bgCore = ["#6098b2", "#229e8f"];
  bgLevel = "#304149";
  bgElectrons = "#0eeda2";

  lnLevel = 2;

  dimCore = 50; // Radio del núcleo
  dimCorePlusElectrons = 50; // Radio extra a partir del nucleo
  dimElectrons = 10; // Radio del electron
  dimElectronsExtra = 5; // Radio extra para colisiones del electron

  bucleSafeLimit = 10000; // Sistema de seguridad anti bucle infinito

  constructor() {
    this.element = new Element();
  }

  bohrLevels() {
    // Creando primero el nucleo

    // Creando un gradiente
    this.ctx.beginPath();
    const grad = this.ctx.createRadialGradient(
      0,
      0,
      this.dimCore,
      0,
      0,
      this.dimCore / 2
    );
    grad.addColorStop(0, this.bgCore[0]);
    grad.addColorStop(1, this.bgCore[1]);

    this.ctx.beginPath();
    this.ctx.fillStyle = grad;
    this.ctx.arc(0, 0, this.dimCore, 0, 2 * Math.PI);
    this.ctx.fill();
    this.ctx.closePath();

    // Agregando la información del núcleo
    this.ctx.font = "bold 11px sans-serif";
    this.ctx.fillStyle = "black";
    this.ctx.fillText(`#p+[${this.element.get_atomic}]`, -25, -20);
    this.ctx.fillText(`#n0[${this.element.get_neutrons(true)}]`, -25, +30);
    this.ctx.font = "bold 15px sans-serif";
    this.ctx.fillText(`[${this.element.get_letter}]`, -17, 5);

    // Creando los niveles
    for (let i = 1; i <= Object.keys(this.levelsElectrons).length; i++) {
      this.ctx.beginPath();
      this.ctx.strokeStyle = this.bgLevel;
      this.ctx.lineWidth = this.lnLevel;
      this.ctx.setLineDash([10, 10]);
      const dimLevel = this.dimCore + i * this.dimCorePlusElectrons;
      this.ctx.arc(0, 0, dimLevel, 0, 2 * Math.PI);
      this.ctx.stroke();
      this.ctx.closePath();

      // Agregando la información del nivel
      const keyLevel = Object.keys(this.levelsElectrons)[i - 1];
      this.ctx.font = "bold 13px sans-serif";
      this.ctx.fillStyle = "black";
      this.ctx.fillText(`${keyLevel}`, 0, -dimLevel - 14);

      // Insertando los electrones
      // Color Random
      // const randomColor = Math.floor(Math.random() * 16777215).toString(16);
      const randomColor = "hsl(" + Math.random() * 360 + ", 100%, 75%)";
      const numE = this.levelsElectrons[keyLevel];
      // Guardamos el rango inferior del radio (Se va a ir actualizando para no
      // repetir)
      let dimLevelNegative = -dimLevel;

      let xPositionSaved = [];
      for (let j = 1; j <= numE; j++) {
        // Cuerpo del electron
        this.ctx.beginPath();
        this.ctx.fillStyle = randomColor;

        let ePosX = 0;
        let ePosY = 0;

        let counter = 0;
        while (true) {
          // Sistema de seguridad
          if (counter == this.bucleSafeLimit) {
            console.error("SEGURIDAD ANTI BUCLES INFINITOS");
            break;
          }

          // Estableciendo los rangos para obtener un número random
          ePosX =
            Math.random() * (dimLevel + 1 - dimLevelNegative) +
            dimLevelNegative;

          // Aplicando formula de la ecuación de la circunferencia x2 + y2 = r2
          // cuando el centro esta en 0,0
          ePosY = Math.sqrt(Math.pow(dimLevel, 2) - Math.pow(ePosX, 2));
          // Validando si el punto es parte de la circunferencía

          // Sacando un volado para saber si serán negativos o positivos
          const eSignal = Math.random() * (10 + 1 - -10) + -10;
          if (eSignal < 0) {
            ePosX = -ePosX;
            ePosY = -ePosY;
          }

          // Validando que el punto este dentro de la circunferencia
          if (
            Math.pow(ePosX, 2) + Math.pow(ePosY, 2) ==
            Math.pow(dimLevel, 2)
          ) {
            // Validando que el punto sea único
            let uniquePos = true;
            xPositionSaved.forEach((posSaved) => {
              if (ePosX == posSaved[0]) {
                uniquePos = false;
                return;
              }
            });
            if (uniquePos) {
              // Validando el sistema de colisión

              if (!xPositionSaved.includes(ePosX)) {
                // Validando el sistema de colisión
                uniquePos = true;
                xPositionSaved.forEach((posSaved) => {
                  const maxSPosX =
                    posSaved[0] + this.dimElectrons + this.dimElectronsExtra;
                  const minSPosX =
                    posSaved[0] - this.dimElectrons - this.dimElectronsExtra;
                  const valPosX = ePosX >= minSPosX && ePosX <= maxSPosX;

                  const maxSPosY =
                    posSaved[1] + this.dimElectrons + this.dimElectronsExtra;
                  const minSPosY =
                    posSaved[1] - this.dimElectrons - this.dimElectronsExtra;
                  const valPosY = ePosY >= minSPosY && ePosY <= maxSPosY;

                  if (valPosX && valPosY) {
                    // console.error("COLISION");
                    uniquePos = false;
                    return;
                  }
                });

                if (uniquePos) {
                  xPositionSaved.push([ePosX, ePosY]);

                  // console.log(ePosX, ePosY);
                  break;
                }
              }
            }
          }

          counter++;
        }

        /*
         * DEPRECATED
        for (let x = dimLevelNegative; x <= dimLevel; x++) {
          ePosX = x;
          // Aplicando formula de la ecuación de la circunferencia x2 + y2 = r2
        cuando el centro esta en 0,0 ePosY = Math.sqrt(Math.pow(dimLevel, 2) -
        Math.pow(ePosX, 2));

          // Validando si el punto es parte de la circunferencía
          if (
            Math.pow(ePosX, 2) + Math.pow(ePosY, 2) ==
            Math.pow(dimLevel, 2)
          ) {
            dimLevelNegative = ePosX + 1;
            //console.log(dimLevelNegative, ePosX, ePosY)
            break;
          }
        }
        */

        this.ctx.arc(ePosX, ePosY, this.dimElectrons, 0, 2 * Math.PI);
        this.ctx.fill();
        this.ctx.closePath();

        this.ctx.font = "bold 12px sans-serif";
        this.ctx.fillStyle = "black";
        this.ctx.fillText(`e-`, ePosX - 5, ePosY + 5);
      }
    }

    // Regresando el origen al original
    this.ctx.restore();
  }

  bohrStage() {
    // Preparar la rotación
    // let g = 0;

    // if (g <= 360) {
    // let rad = (Math.PI * g) / 180;
    // this.ctx.rotate(-rad);

    this.ctx.beginPath();
    this.ctx.fillStyle = this.bgStage;
    this.ctx.fillRect(0, 0, this.width, this.height);
    this.ctx.closePath();

    this.ctx.save();

    // Moviendo el origen al centro
    this.ctx.translate(this.width / 2, this.height / 2);

    this.bohrLevels();

    // g+=1
    //}
  }

  initBohr() {
    this.bohrStage();
    // const animation = setInterval(() => {
    //}, 100)
  }

  generateTitle() {
    const title = document.createElement('h3')
    title.textContent = 'Orbitales y sus Electrones'
    this.bohr_header.appendChild(title)

    const subTitle = document.createElement('div')
    subTitle.classList.add('content__models__bohr__header__formula')
    subTitle.textContent = '2m^2'
    this.bohr_header.appendChild(subTitle)
  }

  generateTable() {
    let counter = 0;

    // Generando Header
    const rowHeader = document.createElement("div");
    rowHeader.classList.add("content__models__bohr__levels__row");
    rowHeader.classList.add("content__models__bohr__levels__row--animation");
    const header = ["Orbital", "Total", "Valor"];
    header.forEach((data, index) => {
      const span = document.createElement("span");
      if (index == 0)
        span.classList.add("content__models__bohr__levels__row__label");
      else if (index == 1)
        span.classList.add("content__models__bohr__levels__row__total");
      else if (index == 2) {
        span.classList.add("content__models__bohr__levels__row__value");
        span.classList.add("content__models__bohr__levels__row__value--title");
      }

      span.textContent = data;

      rowHeader.appendChild(span);
    });

    this.bohr_levels.appendChild(rowHeader);

    const levelsTable = ["1", "2", "3", "4", "5", "6", "7"];

    levelsTable.forEach((data, index) => {
      const rowLevel = document.createElement("div");
      rowLevel.classList.add("content__models__bohr__levels__row");
      rowLevel.classList.add("content__models__bohr__levels__row--animation");
      for (let i = 0; i < 3; i++) {
        const span = document.createElement("span");
        if (i == 0) {
          span.classList.add("content__models__bohr__levels__row__label");
          span.textContent = data;
        } else if (i == 1)
          span.classList.add("content__models__bohr__levels__row__total");
        else if (i == 2) {
          if (index % 2)
            span.classList.add("content__models__bohr__levels__row__value--odd");
          else
            span.classList.add("content__models__bohr__levels__row__value--even");

          span.classList.add("content__models__bohr__levels__row__value");
        }

        rowLevel.appendChild(span);
      }
      this.bohr_levels.appendChild(rowLevel);
    });

    let electrons = this.element.get_atomic;

    Object.keys(this.bohr_levels.children).forEach((row) => {
      if (counter > 0) {
        // Añadiendo la letra del nivel
        let letter = this.bohr_levels.children[row].children[0].textContent;

        // Añadiendo el Total
        let node = this.bohr_levels.children[row].children[1];
        const total = 2 * Math.pow(row, 2);
        node.textContent = total;

        // Añadiendo el Valor
        node = this.bohr_levels.children[row].children[2];
        if (electrons >= total) {
          electrons = electrons - total;
          node.textContent = total;
          this.levelsElectrons[letter] = total;
        } else if (electrons > 0) {
          node.textContent = electrons;
          this.levelsElectrons[letter] = electrons;
          electrons = 0;
        } else node.textContent = "-";
      }

      counter++;
    });
  }
}
class CanvasBohrListeners extends CanvasBohrActions {
  constructor() {
    super();
  }

  btn_bohr_click() {
    this.btn_bohr.addEventListener("click", () => {
      const blank = document.createElement("canvas");
      blank.width = this.width;
      blank.height = this.height;

      if (this.canvas == null)
        this.btn_bohr.textContent = "regenerar";
      else {
        if (this.canvas.toDataURL() == blank.toDataURL())
          this.btn_bohr.textContent = "regenerar";
      }

      this.levelsElectrons = {};

      new Promise((resolve) => {
        try {
          this.bohr_levels.innerHTML = "";
          this.bohr_header.innerHTML = "";
          this.model_bohr.removeChild(this.boxCanvas);
        } catch (err) {}


        this.boxCanvas = document.createElement('div');
        this.boxCanvas.id = "box_bohr"
        this.boxCanvas.classList.add('content__models__bohr__canvas--box')
        this.model_bohr.appendChild(this.boxCanvas)

        this.canvas = document.createElement("canvas");
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.canvas.id = "canvas_bohr";
        this.canvas.classList.add("content__models__bohr__canvas");
        this.ctx = this.canvas.getContext("2d");
        this.boxCanvas.appendChild(this.canvas);


        // Centrar BOX
        const scrollto = this.boxCanvas.offsetLeft * 1.3
        const scrolltoV = this.boxCanvas.scrollHeight / 2 - this.boxCanvas.offsetTop
        this.boxCanvas.scrollLeft = scrollto
        this.boxCanvas.scrollTop = scrolltoV

        resolve();
      }).then((data) => {
        this.generateTitle();
        this.generateTable();
        this.initBohr();
      });
    });
  }
}
class CanvasBohr extends CanvasBohrListeners {
  canvas = null;

  width = 630;
  height = 700;

  btn_bohr = null;

  bohr_header = null
  bohr_levels = null;
  ctx = null;

  model_bohr = null;
  boxCanvas = null

  constructor() {
    super();

    this.model_bohr = document.getElementById("model_bohr");

    this.btn_bohr = document.getElementById("btn_bohr");
    this.btn_header = document.getElementById("bohr_header");
    if (this.canvas == null)
      this.btn_bohr.disabled = true
    this.btn_bohr_click();


    this.bohr_levels = document.getElementById("bohr_levels");
    this.bohr_header = document.getElementById("bohr_header");


    // this.pruebitas();
    // this.canvasListener();
    // this.reloj()
    // this.relojHoras()
  }

  reloj() {
    const ctx = this.canvas.getContext("2d");

    // Agregar fondo
    const grad = ctx.createLinearGradient(150, 0, 150, 300);

    for (let i = 0; i < 1; i += 0.2) {
      grad.addColorStop(i, "lightgreen");
      grad.addColorStop(i + 0.01, "green");
    }

    ctx.fillStyle = grad;
    ctx.fillRect(0, 0, 300, 300);
    // Transladamos el origen al centro del cambas (Ya no será en la posición 0,
    // 0 el origen)
    ctx.translate(150, 150);

    setInterval(() => {
      this.relojCirculo(ctx);
      this.relojHoras(ctx);
    }, 1000);
  }

  relojCirculo(ctx) {
    // Creando el reloj
    ctx.beginPath();
    const grad2 = ctx.createRadialGradient(0, 0, 30, 0, 0, 150);
    grad2.addColorStop(0, "#222222");
    grad2.addColorStop(1, "gray");

    ctx.arc(0, 0, 140, 0, 2 * Math.PI);
    ctx.strokeStyle = "navy";
    ctx.fillStyle = grad2;
    ctx.lineWidth = 3;
    ctx.stroke();
    ctx.fill();
    ctx.save(); // Guardamos el estado antes de empezar a rotar

    // Colocar las marcas
    // Se dibujará una marca cada 6 grados
    for (let i = 0; i < 360; i += 6) {
      const ang = (i * Math.PI) / 180; // Esto sería el valor del angulo en RADIANES
      const rad = (6 * Math.PI) / 180;

      // Colocar la márca de hora, solo si num de grados es divisible por 30
      if (i % 30 == 0) {
        // Aqui se coloca el número
        let num = i / 30 + 6;
        if (num > 12) {
          num -= 12;
        }

        ctx.beginPath();
        ctx.save(); // Guardamos antes de cambiar el origen
        ctx.translate(0, 120); // Tomando como partida que el origen es el
        // centro del circulo es decir 150, 150
        ctx.rotate(-ang);
        ctx.font = "bold 15px arial";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle"; // Centrado vertical
        ctx.fillText(num, 0, 0);
        ctx.restore(); // Devolver origen al centro del circulo
      } else {
        // Aqui se coloca la rayita
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.moveTo(0, 125);
        ctx.lineTo(0, 130);
        ctx.stroke();
      }

      // ROTAMOS
      ctx.rotate(rad);
    }

    ctx.restore(); // Devolvemos el centro al canvas original
  }

  relojHoras(ctx) {
    // Horas
    const date = new Date();
    const hou = date.getHours();
    const min = date.getMinutes();
    const sec = date.getSeconds();
    ctx.save();

    ctx.rotate(Math.PI); // Giramos 180°
    let gr = (hou + min / 60) * 30;
    let rad = (gr * Math.PI) / 180;
    // Manecilla horas
    this.relojSaetas(ctx, rad, 90, 10);

    // Manecilla minutos
    gr = min * 6;
    rad = (gr * Math.PI) / 180;
    this.relojSaetas(ctx, rad, 110, 7);

    // Manecilla segundos
    gr = sec * 6;
    rad = (gr * Math.PI) / 180;
    this.relojSaetas(ctx, rad, 110, 3);

    ctx.restore();
  }

  relojSaetas(ctx, rad, largo, grueso) {
    ctx.save();
    ctx.beginPath();
    ctx.lineWidth = grueso;
    ctx.rotate(rad);
    ctx.moveTo(0, 0);
    ctx.lineTo(0, largo);
    ctx.stroke();
    ctx.restore();
  }

  canvasListener() {
    this.canvas.addEventListener("click", (e) => {
      const rect = this.canvas.getBoundingClientRect();

      const x = e.x - rect.left;
      const y = e.y - rect.top;
      console.log(x, y);
    });
  }

  pruebitas() {
    // Dibujar lineas
    const ctx = this.canvas.getContext("2d");

    // FONDO
    let g = 0;
    let x = 150;
    let y = 150;

    const animation = setInterval(() => {
      ctx.beginPath();
      ctx.fillStyle = "green";
      ctx.fillRect(10, 10, 280, 280);

      ctx.save();

      if (g < 360) {
        ctx.save();
        ctx.translate(x, y); // Muevo el origen al centro
        let rad = (Math.PI * g) / 180;
        ctx.rotate(-rad);

        ctx.beginPath();
        ctx.fillStyle = "cyan";
        // ctx.fillRect(40, 40, 20, 20);
        ctx.arc(0, 40, 10, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();
        ctx.beginPath();
        ctx.fillStyle = "orange";
        // ctx.fillRect(40, 40, 20, 20);
        ctx.arc(0, -40, 10, 0, 2 * Math.PI);
        ctx.closePath();
        ctx.fill();

        g += 3;
        ctx.restore();
      } else {
        clearInterval(animation);
      }
    }, 100);

    // Restore the default state
    // ctx.restore();

    return;
    ctx.beginPath();
    ctx.moveTo(this.width / 2, this.height / 2);
    ctx.lineTo(this.width, this.height);
    ctx.strokeStyle = "white";
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(this.width / 2, this.height / 2);
    ctx.lineTo(0, 0);
    ctx.strokeStyle = "red";
    ctx.stroke();

    // Dibujando Rectangulos
    ctx.beginPath();
    ctx.strokeStyle = "yellow";
    ctx.strokeRect(100, 100, 100, 100);
    ctx.fillStyle = "yellow";
    ctx.fillRect(105, 105, 90, 90);
    ctx.clearRect(110, 110, 80, 80);

    // Dibujando triangulos
    ctx.beginPath();
    ctx.strokeStyle = "pink";
    ctx.moveTo(300, 50);
    ctx.lineTo(400, 50);
    ctx.lineTo(350, 100);
    ctx.closePath();
    ctx.stroke();

    // Dibujando circulos
    // ctx.beginPath();
    // ctx.fillStyle = "cyan";
    // ctx.arc(100, 300, 10, 0, 2 * Math.PI);
    // ctx.fill();

    // Animacion
    // const time = new Date();
    // ctx.rotate(
    //  ((2 * Math.PI) / 60) * time.getSeconds() +
    //    ((2 * Math.PI) / 60000) * time.getMilliseconds()
    //);

    // let a = 10;
    // setInterval(() => {
    //  if (a > 0) {
    //    ctx.beginPath();
    //    ctx.translate(300, 250);
    //    ctx.rotate(Math.PI / a);
    //    ctx.fillRect(0, 0, 40, 40);
    //    ctx.restore()
    //    a--;
    //    console.log(a);
    //  }
    //}, 300);

    // setInterval(() => {
    //  ctx.beginPath();
    //  ctx.fillStyle = "cyan";
    //  ctx.arc(300, 300, 10, 0, 2 * Math.PI);
    //  ctx.fill();
    //  ctx.rotate(2);
    //  ctx.translate(300, 300)
    //  ctx.restore()

    //}, 300);
  }
}
new CanvasBohr();

/*
 * Clases para el área del Quantum
 */
class CanvasQuantumActions {
  arrayTags = [];

  nobleElements = [{
      l: "He",
      e: 2
    },
    {
      l: "Ne",
      e: 10
    },
    {
      l: "Ar",
      e: 18
    },
    {
      l: "Kr",
      e: 36
    },
    {
      l: "Xe",
      e: 54
    },
    {
      l: "Rn",
      e: 86
    },
  ];

  contentTags = [
    // 1
    [{
      l: "s",
      e: 2,
      o: 1,
    }, ],
    // 2
    [{
        l: "s",
        e: 2,
        o: 2,
      },
      {
        l: "p",
        e: 6,
        o: 3,
      },
    ],
    // 3
    [{
        l: "s",
        e: 2,
        o: 4,
      },
      {
        l: "p",
        e: 6,
        o: 5,
      },
      {
        l: "d",
        e: 10,
        o: 7,
      },
    ],
    // 4
    [{
        l: "s",
        e: 2,
        o: 6,
      },
      {
        l: "p",
        e: 6,
        o: 8,
      },
      {
        l: "d",
        e: 10,
        o: 10,
      },
      {
        l: "f",
        e: 14,
        o: 13,
      },
    ],
    // 5
    [{
        l: "s",
        e: 2,
        o: 9,
      },
      {
        l: "p",
        e: 6,
        o: 11,
      },
      {
        l: "d",
        e: 10,
        o: 14,
      },
      {
        l: "f",
        e: 14,
        o: 17,
      },
    ],
    // 6
    [{
        l: "s",
        e: 2,
        o: 12,
      },
      {
        l: "p",
        e: 6,
        o: 15,
      },
      {
        l: "d",
        e: 10,
        o: 18,
      },
      {
        l: "f",
        e: 14,
        o: 20,
      },
    ],
    // 7
    [{
        l: "s",
        e: 2,
        o: 16,
      },
      {
        l: "p",
        e: 6,
        o: 19,
      },
      {
        l: "d",
        e: 10,
        o: 21,
      },
      {
        l: "f",
        e: 14,
        o: 22,
      },
    ],
  ];

  restArrows = null;
  restArrowsB = null;

  generate_header() {
    const title = document.createElement('h3')
    title.textContent = 'Tabla Moeller'
    this.info_header.appendChild(title)

    const table = document.createElement('div')
    table.classList.add('content__models__quantum__information__table')
    table.id = "quantum_table"
    this.info_header.appendChild(table)
  }

  generate_tag(level, letter, value, config = false, table = false) {
    const tag = document.createElement("div");
    tag.classList.add("content__models__quantum__information__table__col");
    if (table)
      tag.classList.add(
        "content__models__quantum__information__table__col--table"
      );
    if (config)
      tag.classList.add(
        "content__models__quantum__information__table__col--config"
      );

    const tagLevel = document.createElement("span");
    tagLevel.classList.add(
      "content__models__quantum__information__table__col__level"
    );
    tagLevel.textContent = level;
    tag.appendChild(tagLevel);

    const tagLetter = document.createElement("span");
    tagLetter.classList.add(
      "content__models__quantum__information__table__col__letter"
    );
    if (value >= 10) {
      tagLetter.classList.add(
        "content__models__quantum__information__table__col__letter--space"
      );
    }
    tagLetter.textContent = letter;
    tag.appendChild(tagLetter);

    const tagValue = document.createElement("span");
    tagValue.classList.add(
      "content__models__quantum__information__table__col__value"
    );

    tagValue.textContent = value;
    tag.appendChild(tagValue);

    return tag;
  }

  generate_table() {
    const rowHeader = document.createElement("div");
    rowHeader.classList.add(
      "content__models__quantum__information__table__row"
    );

    const headerTitle = ["s", "p", "d", "f"];

    for (let i = 0; i < 4; i++) {
      const col = document.createElement("span");
      col.classList.add("content__models__quantum__information__table__col");
      col.classList.add(
        "content__models__quantum__information__table__col--header"
      );
      col.textContent = headerTitle[i];

      rowHeader.appendChild(col);
    }
    this.info_table.appendChild(rowHeader);

    let counter = 1;
    this.contentTags.forEach((tag) => {
      const row = document.createElement("div");
      row.classList.add("content__models__quantum__information__table__row");

      tag.forEach((data) => {
        const data2 = {
          ...data
        };
        data2.level = counter;
        this.arrayTags.push(data2);
        row.appendChild(
          this.generate_tag(counter, data.l, data.e, false, true)
        );
      });

      this.info_table.appendChild(row);

      counter++;
    });
  }

  generate_title_element(titleText, simple = false) {
    const title = document.createElement("span");
    title.classList.add(
      "content__models__quantum__information__operations__configuration__title"
    );
    if (simple)
      title.classList.add(
        "content__models__quantum__information__operations__configuration__title--simple"
      );
    title.textContent = titleText;

    return title;
  }

  double_arrow(node) {
    for (let j = 1; j <= 2; j++) {
      // Dibujado las flechas
      const arrow = document.createElement("span");
      arrow.classList.add(
        "content__models__quantum__information__operations__configuration__frac__arrows__arrow"
      );

      if (j % 2 != 0) {
        arrow.classList.add(
          "content__models__quantum__information__operations__configuration__frac__arrows__arrow--reverse"
        );
      }

      node.appendChild(arrow);
    }
  }

  generate_fracc(tag, value) {
    let repeat = 1;

    switch (tag.l) {
      case "p":
        repeat = 3;
        break;
      case "d":
        repeat = 5;
        break;
      case "f":
        repeat = 7;
        break;
    }

    const box = document.createElement("div");
    box.classList.add(
      "content__models__quantum__information__operations__configuration__frac"
    );

    let numberArrows = value; // Esta variable la ocupo para testing
    this.restArrows = value;
    this.restArrowsB = numberArrows - repeat;
    for (let i = 0; i < repeat; i++) {
      const boxCard = document.createElement("div");
      boxCard.classList.add(
        "content__models__quantum__information__operations__configuration__frac--card"
      );

      const arrows = document.createElement("span");
      arrows.classList.add(
        "content__models__quantum__information__operations__configuration__frac__arrows"
      );
      boxCard.appendChild(arrows);

      if (numberArrows == repeat * 2) {
        this.double_arrow(arrows);
      } else {
        if (numberArrows <= repeat) {
          // Cuando hay menos flechas que bloques
          if (this.restArrows > 0) {
            const arrow = document.createElement("span");
            arrow.classList.add(
              "content__models__quantum__information__operations__configuration__frac__arrows__arrow"
            );

            arrows.appendChild(arrow);
          }

          this.restArrows--;
        } else if (numberArrows < repeat * 2) {
          if (this.restArrowsB > 0) {
            this.double_arrow(arrows);
          } else {
            const arrow = document.createElement("span");
            arrow.classList.add(
              "content__models__quantum__information__operations__configuration__frac__arrows__arrow"
            );

            arrows.appendChild(arrow);
          }
          this.restArrowsB--;
        } else {
          console.error("Hubo un error con la lógica del ejercicio ;(");
        }
      }

      const line = document.createElement("span");
      line.classList.add(
        "content__models__quantum__information__operations__configuration__frac__line"
      );
      boxCard.appendChild(line);

      const label = document.createElement("span");
      label.classList.add(
        "content__models__quantum__information__operations__configuration__frac__label"
      );
      boxCard.appendChild(label);
      const labelLevel = document.createElement("span");
      labelLevel.classList.add(
        "content__models__quantum__information__operations__configuration__frac__label__level"
      );
      labelLevel.textContent = tag.level;
      label.appendChild(labelLevel);
      const labelLetter = document.createElement("span");
      labelLetter.classList.add(
        "content__models__quantum__information__operations__configuration__frac__label__letter"
      );
      labelLetter.textContent = tag.l;
      label.appendChild(labelLetter);

      box.appendChild(boxCard);
    }

    return box;
  }

  generate_operations() {
    // Titulo
    const ope_title = document.createElement("h3");
    ope_title.textContent = "Configuración Electronica";

    this.info_operations.appendChild(ope_title);

    // Configuración Electronica Larga
    const ope_conf = document.createElement("div");
    ope_conf.classList.add(
      "content__models__quantum__information__operations__configuration"
    );
    ope_conf.id = "quantic_conf_large";

    ope_conf.appendChild(this.generate_title_element(this.element.get_letter));

    // Configuracion Electronica Ordenada
    let numE = this.element.get_atomic;
    const largeConfig = [];
    for (let i = 1; i <= this.arrayTags.length; i++) {
      const tag = this.arrayTags.find((tag) => {
        return tag.o == i;
      });

      // Rellenar solo hasta cumplir los electrones
      if (numE >= tag.e) {
        largeConfig.push(tag);
        ope_conf.appendChild(this.generate_tag(tag.level, tag.l, tag.e, true));
        numE = numE - tag.e;
      } else if (numE > 0) {
        largeConfig.push(tag);
        ope_conf.appendChild(this.generate_tag(tag.level, tag.l, numE, true));
        numE = 0;
      }
    }

    this.info_operations.appendChild(ope_conf);

    // Configuración Electronica Corta
    const ope_conf_short = document.createElement("div");
    ope_conf_short.classList.add(
      "content__models__quantum__information__operations__configuration"
    );

    ope_conf_short.appendChild(
      this.generate_title_element(this.element.get_letter)
    );

    // Encontrando el elemento anterior en electrones
    const noble = [];

    this.nobleElements.find((data) => {
      if (data.e <= this.element.get_atomic) noble.push(data);
    });

    // Esto es para cuando se seleccione el Hidrogeno y no genere un fallo
    let nobleLast = []
    if (noble.length == 0) {
      ope_conf_short.appendChild(this.generate_title_element(this.arrayTags[0].L, true));
    } else {
      nobleLast = noble.pop();
      ope_conf_short.appendChild(this.generate_title_element(nobleLast.l, true));
    }

    numE = nobleLast.e;
    const shortConfig = [];
    for (let i = 1; i <= this.arrayTags.length; i++) {
      const tag = this.arrayTags.find((tag) => tag.o == i);

      // Eliminando configuraciones de la larga
      if (numE >= tag.e) {
        shortConfig.push(tag);
        numE = numE - tag.e;
      } else if (numE > 0) {
        shortConfig.push(tag);
        numE = 0;
      }
    }

    // Combinando arreglos
    numE = this.element.get_atomic - nobleLast.e;
    const fullConfig = largeConfig.concat(shortConfig);
    // Eliminando las copias
    const unique = fullConfig.filter((data, index, self) => {
      const same = self.filter((data2) => {
        return data2.o == data.o;
      });

      if (same.length == 1) {
        if (numE >= same[0].e) {
          ope_conf_short.appendChild(
            this.generate_tag(same[0].level, same[0].l, same[0].e, true)
          );
          numE = numE - same[0].e;
        } else if (numE > 0) {
          ope_conf_short.appendChild(
            this.generate_tag(same[0].level, same[0].l, numE, true)
          );
          numE = 0;
        }

        return same;
      }
    });
    this.info_operations.appendChild(ope_conf_short);

    // Agregando las fracciones
    const fracc = document.createElement("div");
    fracc.classList.add(
      "content__models__quantum__information__operations__configuration__frac--content"
    );
    fracc.id = "frac_cards";

    numE = this.element.get_atomic;
    largeConfig.forEach((data) => {
      let value = 0;
      if (numE >= data.e) {
        numE = numE - data.e;
        value = data.e;
      } else if (numE > 0) {
        value = numE;
        // value = 2; // TESTING
        numE = 0;
      }

      fracc.appendChild(this.generate_fracc(data, value));
    });

    this.info_operations.appendChild(fracc);
  }

  textConf(level, letter, value, x, y, x2, y2, x3, y3, Level = true, Config = true) {
    this.ctx.font = "bold 13px sans-serif";
    this.ctx.fillStyle = "black";
    if (Level) this.ctx.fillText(`n = ${level}`, x, y);

    if (Config) {
      this.ctx.fillText(`${level}${letter}`, x2, y2);
      this.ctx.font = "bold 10px sans-serif";
      this.ctx.fillText(`${value}`, x3, y3);
    }
  }

  textFrac(letter, arrows, x, y) {
    // Linea
    this.ctx.beginPath();
    this.ctx.strokeStyle = "black";
    this.ctx.lineWidth = this.lineWidth;
    this.ctx.moveTo(x, y - 3);
    this.ctx.lineTo(x + 20, y - 3);
    this.ctx.stroke();
    this.ctx.closePath();

    // Texto
    this.ctx.font = "bold 13px sans-serif";
    this.ctx.fillStyle = "black";
    this.ctx.fillText(`${letter}`, x, y + 10);

    // Flechas
    for (let i = 0; i < arrows; i++) {
      const xl = x + 5 * (i + 1) * 2 - 5;
      const yl = y - 5;
      const yl2 = y - 3 - 20;

      // Dibujando la linea
      this.ctx.beginPath();
      this.ctx.strokeStyle = "black";
      this.ctx.lineWidth = this.lineWidth;
      this.ctx.moveTo(xl, yl);
      this.ctx.lineTo(xl, yl2);
      this.ctx.stroke();
      this.ctx.closePath();

      // Dibujando el triangulo

      let xyT = [x + 5, y - 25];
      let xyT2 = [x - 5 + 5, y + 8 - 25];
      let xyT3 = [x + 5 + 5, y + 8 - 25];
      if ((i + 1) % 2 == 0) {
        // xyT = [x + 15, y - 15];
        // xyT2 = [x - 5 + 15, y + 8 - 15];
        // xyT3 = [x + 5 + 15, y + 8 - 15];

        xyT = [x + 15, y - 5];
        xyT2 = [x - 5 + 15, y - 8 - 5];
        xyT3 = [x + 5 + 15, y - 8 - 5];
      }

      this.ctx.beginPath();
      this.ctx.fillStyle = "black";
      this.ctx.moveTo(xyT[0], xyT[1]);
      this.ctx.lineTo(xyT2[0], xyT2[1]);
      this.ctx.lineTo(xyT3[0], xyT3[1]);
      this.ctx.closePath();
      this.ctx.fill();
    }
  }

  quanticOperation() {
    // this.ctx.beginPath();
    // const grad = this.ctx.createRadialGradient(0, 0, 4, 0, 0, 2);
    // grad.addColorStop(0, this.bgCore[0]);
    // grad.addColorStop(1, this.bgCore[1]);

    // this.ctx.beginPath();
    // this.ctx.fillStyle = grad;
    // this.ctx.arc(0, 0, this.dimCore, 0, 2 * Math.PI);
    // this.ctx.fill();
    // this.ctx.closePath();

    // Consiguiendo la información de la configuración cuantica
    const conf = document.getElementById("quantic_conf_large").children;
    const confTreat = [];
    Object.keys(conf).forEach((i) => {
      if (i > 0) {
        const dataLevel = conf[i].children[0].textContent;
        const dataLetter = conf[i].children[1].textContent;
        const dataValue = conf[i].children[2].textContent;

        confTreat.push({
          l: dataLevel,
          L: dataLetter,
          v: dataValue
        });
      }
    });

    //console.log(confTreat);

    // Creando las lineas bases del ejercicio
    // Linea horizontal
    this.ctx.beginPath();
    this.ctx.strokeStyle = this.bgMainLines;
    this.ctx.lineWidth = this.widthMainLine;
    this.ctx.moveTo(-(this.width / 4), 0);
    this.ctx.lineTo(this.width / 4, 0);
    this.ctx.stroke();
    this.ctx.closePath();

    // Linea Vertical
    const maxHeightLine = this.height - this.marginBottomCanvas * 2;
    this.ctx.beginPath();
    this.ctx.strokeStyle = this.bgMainLines;
    this.ctx.lineWidth = this.widthMainLine;
    this.ctx.moveTo(0, 0);
    this.ctx.lineTo(0, -maxHeightLine);
    this.ctx.stroke();
    this.ctx.closePath();

    // Creando el nivel 1
    this.textConf(
      confTreat[0].l,
      confTreat[0].L,
      confTreat[0].v,
      -(this.width / 4 + 50),
      0,
      this.width / 4 - 50,
      20,
      this.width / 4 - 33,
      15
    );

    this.textFrac(
      `${confTreat[0].l}${confTreat[0].L}`,
      confTreat[0].v,
      this.width / 4 + 20,
      0
    );

    // this.ctx.beginPath();
    // this.ctx.strokeStyle = this.bgMainLines;
    // this.ctx.lineWidth = this.widthMainLine;
    // this.ctx.moveTo(0, 0);
    // this.ctx.lineTo(0, -maxHeightLine);
    // this.ctx.stroke();
    // this.ctx.closePath();

    // Creando las divisiones de los niveles
    let maxLevel = 0;
    const childLines = {};
    confTreat.forEach((data) => {
      if (childLines[data.l] >= 0) {
        childLines[data.l] = childLines[data.l] + 1;
      } else childLines[data.l] = 1;
      if (data.l > maxLevel) maxLevel = data.l;
    });

    this.ctx.save();

    const divLevel = maxHeightLine / (maxLevel - 1);
    let c = 1;

    // Trayendo las cards
    const cards = document.getElementById("frac_cards").children;

    let minLevel = 1

    for (let i = minLevel; i <= maxLevel; i++) {
      if (i > confTreat.length - 1)
        break;

      this.ctx.save();
      this.ctx.translate(0, -divLevel * i);

      this.ctx.beginPath();
      this.ctx.strokeStyle = this.bgLines;
      this.ctx.lineWidth = this.lineWidth;
      this.ctx.moveTo(-(this.width / 4), 0);
      this.ctx.lineTo(0, 0);
      this.ctx.stroke();
      this.ctx.closePath();

      // Ordenamiento
      const cL = [];

      confTreat.forEach((data) => {
        if (i + 1 == data.l) cL.push(data);
      });

      // Ordenando Cards
      const cardsSort = {};
      let s = 0;
      Object.keys(cards).forEach((index) => {
        const cardLetter = cards[index].children[0].children[2].textContent;
        if (i + 1 == cardLetter.charAt(0)) {
          cardsSort[cardLetter] = [];
          s++;
        }
      });
      s = 0;
      Object.keys(cards).forEach((index) => {
        const cardLetter = cards[index].children[0].children[2].textContent;
        if (i + 1 == cardLetter.charAt(0)) {
          cardsSort[cardLetter] = [];
          Object.keys(cards[index].children).forEach((i2) => {
            cardsSort[cardLetter].push(
              cards[index].children[i2].children[0].children.length
            );
          });
          s++;
        }
      });

      for (let j = 1; j <= childLines[i + 1]; j++) {

        let xLevel = this.width / 7;
        let yLevel = -(j * 40 - 100);

        // if (j % 2 == 0) {
        //  yLevel = -yLevel
        //}

        this.ctx.beginPath();
        this.ctx.strokeStyle = this.bgLines;
        this.ctx.lineWidth = this.lineWidth;
        this.ctx.moveTo(0, 0);
        this.ctx.lineTo(xLevel, yLevel);
        this.ctx.stroke();
        this.ctx.closePath();

        this.ctx.save();

        this.ctx.translate(xLevel, yLevel);

        const cLP = cL.shift();

        this.textConf(
          cLP.l,
          cLP.L,
          cLP.v,
          0,
          0,
          -20,
          20,
          -20 + 17,
          15,
          false,
          true
        );

        c++;

        for (let h = 0; h < cardsSort[cLP.l + cLP.L].length; h++) {
          this.textFrac(
            cLP.l + cLP.L,
            cardsSort[cLP.l + cLP.L][h],
            4 * (h + 1) * 7,
            10
          );
        }

        this.ctx.restore();
      }

      let L1 = confTreat[i].L
      let V1 = confTreat[i].v

      this.textConf(
        i + 1,
        L1,
        V1,
        -(this.width / 4 + 50),
        0,
        this.width / 4 - 50,
        20,
        this.width / 4 - 33,
        15,
        true,
        false
      );

      this.ctx.restore();
    }
  }

  quanticStage() {
    this.ctx.beginPath();
    this.ctx.fillStyle = this.bgStage;
    this.ctx.fillRect(0, 0, this.width, this.height);
    this.ctx.closePath();

    this.ctx.save();

    // Moviendo el origen al centro X y Y abajo
    this.ctx.translate(
      this.width / 2,
      this.height - this.widthMainLine - this.marginBottomCanvas
    );

    this.quanticOperation();
  }

  initQuantic() {
    this.quanticStage();
    // const animation = setInterval(() => {
    //}, 100)
  }
}

class CanvasQuantumListeners extends CanvasQuantumActions {
  constructor() {
    super();
  }

  btn_quantum_click() {
    this.btn_quantum.addEventListener("click", () => {
      const blank = document.createElement("canvas");
      blank.width = this.width;
      blank.height = this.height;

      if (this.canvas == null)
        this.btn_quantum.textContent = "regenerar";
      else {
        if (this.canvas.toDataURL() == blank.toDataURL())
          this.btn_quantum.textContent = "regenerar";
      }

      this.info_header.innerHTML = "";
      this.info_operations.innerHTML = "";

      this.arrayTags = [];
      this.restArrows = null;
      this.restArrowsB = null;

      new Promise((resolve) => {
        try {
          this.model_quantic.removeChild(this.boxCanvas);
        } catch (err) {}

        this.boxCanvas = document.createElement('div');
        this.boxCanvas.id = "box_quantic"
        this.boxCanvas.classList.add('content__models__quantic__canvas--box')
        this.model_quantic.appendChild(this.boxCanvas)

        this.canvas = document.createElement("canvas");
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.canvas.id = "canvas_quantic";
        this.canvas.classList.add("content__models__quantic__canvas");
        this.ctx = this.canvas.getContext("2d");
        this.boxCanvas.appendChild(this.canvas);

        // Centrar BOX
        const scrollto = this.boxCanvas.offsetLeft / 8
        const scrolltoV = this.boxCanvas.scrollHeight
        this.boxCanvas.scrollLeft = scrollto
        this.boxCanvas.scrollTop = scrolltoV

        resolve();
      }).then((data) => {
        this.generate_header()
        this.info_table = document.getElementById("quantum_table");
        this.generate_table();
        this.generate_operations();
        this.initQuantic();
      });
    });
  }
}

class CanvasQuantum extends CanvasQuantumListeners {
  bgStage = "lightgray";
  bgCore = ["red", "blue"];

  bgMainLines = "#047555";
  bgLines = "#b7470b";

  widthMainLine = 5;
  lineWidth = 2;
  marginBottomCanvas = 50;

  info_table = null;

  info_operations = null;
  info_header = null

  element = null;

  boxCanvas = null
  canvas = null;
  width = 600;
  height = 1000;
  ctx = null;

  btn_quantum = null;

  model_quantic = null;

  constructor() {
    super();
    this.model_quantic = document.getElementById("model_quantic");
    this.info_header = document.getElementById("quantum_header");
    this.info_operations = document.getElementById("quantum_operations");

    this.btn_quantum = document.getElementById("btn_quantum");
    if (this.canvas == null)
      this.btn_quantum.disabled = true
    this.btn_quantum_click();

    this.element = new Element();
  }
}
new CanvasQuantum();
